package com.mygdx.keepitstreeet;

import com.badlogic.gdx.Game;
import com.mygdx.keepitstreeet.screen.MenuScreen;


public class MainGame extends Game {

    @Override
    public void create() {
        setScreen(new MenuScreen(this));
    }
}
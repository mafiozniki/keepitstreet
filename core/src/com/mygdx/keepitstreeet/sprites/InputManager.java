package com.mygdx.keepitstreeet.sprites;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class InputManager extends InputAdapter {

    private OrthographicCamera camera;
    private static Vector3 temp = new Vector3();

    public InputManager(OrthographicCamera camera) {
        this.camera = camera;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        temp.set(screenX,screenY, 0);
        // получаем координаты касания относительно области просмотра нашей камеры
        camera.unproject(temp);
        float touchX = temp.x;
        float touchY = temp.y;
        GameManager.car.handleTouch(touchX);
        return false;

    }

}
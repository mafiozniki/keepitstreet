package com.mygdx.keepitstreeet.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

class Car {
    Sprite carSprite;

    void render(SpriteBatch batch) {
        carSprite.draw(batch);
    }

    void setPosition(float x) {
        carSprite.setPosition(x, (float) 0);
    }

    void handleTouch(float x) {
        if (x - (carSprite.getWidth() / 2) > 0.0) {
            setPosition(x - (carSprite.getWidth() / 2));
        } else {
            setPosition(0);
        }
    }

}

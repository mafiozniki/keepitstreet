//Куликов Михаил 

package com.mygdx.keepitstreeet.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameManager {
    static Car car;
    private static Texture carTexture;
    private static Sprite backgroundSprite; // спрайт заднего фона
    private static Texture backgroundTexture; // текстура для заднего фона

    public static void initialize(float width,float height){
        car = new Car();
        carTexture = new Texture(Gdx.files.internal("car1_gotovaya.png"));
        car.carSprite = new Sprite(carTexture);
        // переменная для масштабирования машины
        float CAR_RESIZE_FACTOR = 3000f;
        car.carSprite.setSize(car.carSprite.getWidth()*(width/ CAR_RESIZE_FACTOR),
                car.carSprite.getHeight()*(width/ CAR_RESIZE_FACTOR)); // устанавливаем размер спрайта
        car.setPosition(0);// установим позицию машины в левом нижнем углу
        backgroundTexture = new Texture(Gdx.files.internal("background.jpg"));
        backgroundSprite = new Sprite(backgroundTexture);
        backgroundSprite.setSize(width, height); // устанавливаем размер заднего фона по размеру экрана предполагаемого устройства
    }

    public static void renderGame(SpriteBatch batch){

        backgroundSprite.draw(batch);
        car.render(batch);

    }

    public static void dispose() {
        backgroundTexture.dispose();
        carTexture.dispose();
    }
}


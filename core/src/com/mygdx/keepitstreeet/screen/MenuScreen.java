package com.mygdx.keepitstreeet.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.keepitstreeet.KeepItStreet;
import com.mygdx.keepitstreeet.MainGame;


public class MenuScreen implements Screen {

    private SpriteBatch batch; // объект для отрисовки спрайтов нашей игры
    private OrthographicCamera camera; // область просмотра нашей игры

    private Texture startButtonTexture;
    private Sprite startButtonSprite;
    private Sprite backGroundSprite;

    private MainGame game; // экземпляр класса MainGame нужен для доступа к вызову метода setScreen

    private Vector3 temp = new Vector3(); // временный вектор для "захвата" входных координат

    public MenuScreen(MainGame game) {

        this.game = game;

        // получаем размеры экрана устройства пользователя и записываем их в переменнные высоты и ширины
        float height = Gdx.graphics.getHeight();
        float width = Gdx.graphics.getWidth();
        // устанавливаем переменные высоты и ширины в качестве области просмотра нашей игры
        camera = new OrthographicCamera(width, height);
        // этим методом мы центруем камеру на половину высоты и половину ширины
        camera.setToOrtho(false);
        batch = new SpriteBatch();

        // инициализируем текстуры и спрайты
        startButtonTexture = new Texture(Gdx.files.internal("go.png"));
        Texture backGroundTexture = new Texture(Gdx.files.internal("sti.png"));
        startButtonSprite = new Sprite(startButtonTexture);
        backGroundSprite = new Sprite(backGroundTexture);
        // устанавливаем размер и позиции
        // задаём относительный размер
        float BUTTON_RESIZE_FACTOR = 900f;
        startButtonSprite.setSize(startButtonSprite.getWidth() * (width / BUTTON_RESIZE_FACTOR), startButtonSprite.getHeight() * (width / BUTTON_RESIZE_FACTOR));
        backGroundSprite.setSize(width, height);
        // задаём позицию конпки start
        float START_VERT_POSITION_FACTOR = 1.15f;
        startButtonSprite.setPosition((width / 2f - startButtonSprite.getWidth() / 2), width / START_VERT_POSITION_FACTOR);
    }

    private void handleTouch() {
        // Проверяем были ли касание по экрану?
        if (Gdx.input.justTouched()) {
            // Получаем координаты касания и устанавливаем эти значения в временный вектор
            temp.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            // получаем координаты касания относительно области просмотра нашей камеры
            camera.unproject(temp);
            float touchX = temp.x;
            float touchY = temp.y;
            // обработка касания по кнопке Stare
            if ((touchX >= startButtonSprite.getX()) && touchX <= (startButtonSprite.getX() + startButtonSprite.getWidth()) && (touchY >= startButtonSprite.getY()) && touchY <= (startButtonSprite.getY() + startButtonSprite.getHeight())) {
                game.setScreen(new KeepItStreet()); // Переход к экрану игры
            }
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        // Очищаем экран
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);// устанавливаем в экземпляр spritebatch вид с камеры (области просмотра)

        //отрисовка игровых объектов
        batch.begin();
        backGroundSprite.draw(batch);
        startButtonSprite.draw(batch);
        handleTouch();
        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

        dispose();
    }

    @Override
    public void dispose() {

        startButtonTexture.dispose();
        batch.dispose();

    }

}
package com.mygdx.keepitstreeet;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.keepitstreeet.sprites.GameManager;
import com.mygdx.keepitstreeet.sprites.InputManager;


import java.util.Iterator;


public class KeepItStreet implements Screen {
    private Texture raceCar;
    private SpriteBatch batch; // объект для отрисовки спрайтов нашей игры
    private OrthographicCamera camera; // область просмотра нашей игры

    private long lastCarTime;
    private Array<Rectangle> race;


    public KeepItStreet() {
        raceCar = new Texture(Gdx.files.internal("red_car.png"));
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(width, height);// устанавливаем переменные высоты и ширины в качестве области просмотра нашей игры
        camera.setToOrtho(false);// этим методом мы центруем камеру на половину высоты и половину ширины
        batch = new SpriteBatch();
        //вызываем метод initialize класса GameManager
        GameManager.initialize(width, height);
        Gdx.input.setInputProcessor(new InputManager(camera));// доступ класса InputManager для получения касаний/нажатий
        race = new Array<>();
        spawnRacecar();

    }

    private void spawnRacecar() {
        Rectangle racecar = new Rectangle();
        racecar.x = MathUtils.random(0, 1000 - 150);
        racecar.y = 2500;
        racecar.width = 150;
        racecar.height = 150;
        race.add(racecar);
        lastCarTime = TimeUtils.nanoTime();
    }


    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

        dispose();
    }

    @Override
    public void dispose() {
        raceCar.dispose();
        batch.dispose();
        GameManager.dispose();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1, 1, 1, 1);// Очищаем экран
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined); // устанавливаем в экземпляр spritebatch вид с камеры (области просмотра)
        //отрисовка игровых объектов
        batch.begin();
        GameManager.renderGame(batch);
        for (Rectangle racecar : race) {
            batch.draw(raceCar, racecar.x, racecar.y);
        }
        batch.end();

        if (TimeUtils.nanoTime() - lastCarTime > 1000000000) spawnRacecar();

        Iterator<Rectangle> iter = race.iterator();
        while (iter.hasNext()) {
            Rectangle racecar = iter.next();
            racecar.y -= 500 * Gdx.graphics.getDeltaTime();
            if (racecar.y + 500 < 0) iter.remove();
            }
        }
    }
